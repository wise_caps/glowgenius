;l'**Skin Care Recommendation System**
Description

This project is a skin care recommendation system that suggests skincare ingredients based on the user's skin type and skin concerns. It provides personalized recommendations to help users address their specific skin issues.

**Features**
Allows users to input their skin type and skin concerns

Provides a list of skincare ingredients recommended for the user's skin type and concerns

Offers pro tips for skincare based on the user's skin type


**dependencies**
Python

Tkinter for the GUI

SQLite for the database


**Setup**
Clone the repository to your local machine.

Install the required dependencies using pip install -r requirements.txt.

Run the main.py file to start the application.


**How to Use**
Sign up or log in to the system.

Select your skin type and skin concerns.

Click on the "Suggest" button to get personalized skincare ingredient recommendations.

View the recommended ingredients and pro tips for your skin type.


**Contributors**

V.L.S.Harshitha

R.tejaswi

Y.devi saranya

M.bharathi

v.s.l.Meghana
