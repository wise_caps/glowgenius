from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk

class SignUp():
    def landingpage(self) :
        self.user.destroy()
        self.user.entry1.destroy()
        self.password.destroy()
        self.password.entry2.destroy()
        self.button.destroy()

        landing_page = Landing_page(tkob)

    def __init__(self,tkob):
        self.tkob = tkob
        self.user = Label(tkob, text = "Username" , font = ("Ariel", 15, "bold"))
        self.user.place(x =250, y =250)
        self.user.entry1 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.user.entry1.place(x = 400, y = 250)

        self.password = Label(tkob, text = "Password", font = ("Ariel", 15, "bold"))
        self.password.place(x = 250, y = 300)
        self.password.entry2 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.password.entry2.place(x = 400, y = 300)

        self.button = Button(tkob, text = "Submit", font = ("Ariel", 15, "bold"), command = self.landingpage)
        self.button.place(x = 450, y = 400)

class Landing_page():
    
    def next_page(self):
        #print(" Work in Progress......")
        self.name.destroy()
        self.tag.destroy()
        self.bg_img.destroy()
        self.skin_type_label.destroy()
        self.skin_type_dropdown.destroy()
        self.skin_concern_label.destroy()
        self.skin_concern_dropdown.destroy()
        self.suggest.destroy()
        sug = Suggest(tkob)

    def __init__(self, tkob):
        self.tkob = tkob
        self.bgr_img = Image.open('bgr_img.png')
        self.bgr_img = self.bgr_img.resize((950, 650))
        self.bgr_img = ImageTk.PhotoImage(self.bgr_img)

        self.bg_img = Label(tkob, image=self.bgr_img)
        self.bg_img.place(x=0, y=0)

        self.name = Label(tkob, text="Glow Genius", font=('Roman', 50, 'bold'))
        self.name.place(x=300, y=10)
        
        self.tag = Label(tkob, text = " - get the best version of your skin..", font = ('Courier New' ,13))
        self.tag.place(x=550, y=100)

        self.skin_type_label = Label(tkob, text="Skin Type:", font=('Courier New', 15, 'bold'))
        self.skin_type_label.place(x=85, y=180)

        
        self.skin_type_dropdown = ttk.Combobox(tkob, font=('Courier New', 10) )
        self.skin_type_dropdown['values'] = ['Normal', 'Dry', 'Oily','Sensitive','Combination'  ] 
        self.skin_type_dropdown.place(x=215, y=180)
  
        self.skin_concern_label = Label(tkob, text="Skin Concern:", font=('Courier New', 15,'bold'))
        self.skin_concern_label.place(x=500, y=180)

        self.skin_concern_dropdown = ttk.Combobox(tkob, font=('Courier New', 10))
        self.skin_concern_dropdown['values'] = ['Acne', 'Hyper pigmentation', 'Open pores','Wrinkles/fine lines','black/White heads', 'None']
        self.skin_concern_dropdown.place(x=665, y=180)

        self.suggest = Button(tkob,text = "Suggest" , font = ('Times New Roman',15,'bold'),command = self.next_page)
        self.suggest.place(x = 425 , y = 300)

class Suggest():
    def __init__(self,tkob):
        self.tkob = tkob
    
        self.bgr2_img = Image.open('img.png')
        self.bgr2_img = self.bgr2_img.resize((950, 650))
        self.bgr2_img = ImageTk.PhotoImage(self.bgr2_img)

        self.bg2_img = Label(tkob, image=self.bgr2_img)
        self.bg2_img.place(x=0, y=0)

        self.update = Label(tkob,text = "Work in progress ....", font = ('Courier New', 15 ,'bold'))
        self.update.place(x = 300, y = 10)

    

tkob = Tk()
tkob.title("Skin Care")
signup = SignUp(tkob)
tkob.geometry('950x650+250+30')
#landing_page = Landing_page(tkob)
tkob.mainloop()


